const express = require('express');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

app.use(express.json());


//UBICACION DE LOS ENDPOINTS

const usuarios = require('./usuarios');
app.use('/usuarios', usuarios);

const login = require('./login');
app.use('/login', login);

const productos = require('./productos');
app.use('/productos', productos);

const orders = require('./orders');
app.use('/orders', orders);

const paymentMethod = require('./payment-method');
app.use('/payment-method', paymentMethod);

const confirm = require('./confirm-order');
app.use('/confirm-order', confirm);




//-------------------ESCUCHANDO EN PUERTO------------------
app.listen(3000, () => console.log('listening on 3000'));