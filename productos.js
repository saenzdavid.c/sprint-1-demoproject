const express = require ('express');
const router = express.Router();
const productos = require('./data');
const middleware = require('./middleware');

// MUESTRA UNA LISTA DE PRODUCTOS
router.get('/' ,middleware.validarLogeo, (req, res) => {
    res.status('200').json({'Productos':productos.products});
})

// CREA UN PRODUCTO NUEVO
router.post('/',middleware.validarLogeo ,middleware.validarAdmin ,(req, res) => {
    let prodAux = productos.products.find(prodAux => prodAux.nombre === req.body.nombre);
    let newId = productos.products.length;

    if(newId > 0) newId = productos.products[productos.products.length-1].id;

    if(!prodAux){
        productos.products.push({
            id:newId+1,
            nombre: req.body.nombre,
            precio: req.body.precio
        });
        res.status('201').json('Se agrego '+req.body.nombre+' como nuevo producto');
    }else{ 
        return res.status(400).send('Producto ya existente');
    }
})
  
//MODIFICA UN PRODUCTO     ----------->error<---------------
router.put('/:id' ,middleware.validarLogeo, middleware.validarAdmin, (req, res) => {
    const id =  parseInt(req.params.id);
    let idxAux = productos.products.findIndex(idxAux => idxAux.id === id);

    console.log(idxAux);
    
    if(idxAux >= 0){
        productos.products[idxAux].nombre = req.body.nombre;
        productos.products[idxAux].precio = req.body.precio;
        res.status('200').send('Producto modificado');
    }else{
        return res.status(500).send('Producto Inexistente');
    }
})
  
//ELIMINA UN PRODUCTO   ----------->error<---------------
router.delete('/:id' ,middleware.validarLogeo, middleware.validarAdmin, (req, res) => {
    const id =  parseInt(req.params.id);
    let idxAux = productos.products.findIndex(idxAux => idxAux.id === id);

    console.log(idxAux);

    if(idxAux>=0){
        productos.products.splice(idxAux , 1);
    }
    else return res.status(400).send('Producto Inexistente')
    
    res.status('200').send('Producto Eliminado');
})
  
module.exports = router;