const express = require ('express');
const router = express.Router();
const middleware = require('./middleware');
const usuarios = require('./data');


router.get('/'  ,middleware.validarLogeo, middleware.validarAdmin, (req, res) => {
    res.status('200').json({'Usuarios': usuarios.users});
})
  
  
router.post('/', (req, res) => {
    let usuarioAux = usuarios.users.find(usuarioAux => usuarioAux.email === req.body.email);
    let newId = usuarios.users.length;

    if(newId > 0) newId = usuarios.users[usuarios.users.length-1].id;

    if(!usuarioAux){
        usuarios.users.push({
            id:newId+1,
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            email: req.body.email,
            telefono: req.body.telefono,
            userPass: req.body.userPass,
            admin: false,
            login: false
        });
        res.status('201').send('Usuario creado con exito');
        
    }else{
        return res.status(400).send('email ya registrado');
    }
})
  

router.put('/', middleware.validarLogeo, (req, res) => {
    let idxAux = usuarios.users.findIndex(idxAux => idxAux.id === parseInt(req.headers.iduser,10));
    if(idxAux >= 0){
        usuarios.users[idxAux].nombre = req.body.nombre;
        usuarios.users[idxAux].apellido = req.body.apellido;
        usuarios.users[idxAux].email = req.body.email;
        usuarios.users[idxAux].telefono = req.body.telefono;
        usuarios.users[idxAux].userPass = req.body.userPass;
        res.status('200').send('Usuario modificado');
    }else{
        return res.status(500).send('Usuario Inexistente');
    }
})
  


module.exports = router;