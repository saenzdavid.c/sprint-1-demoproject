const data = require('./data');

//VALIDACION DE USUARIO LOGUEADO
function validarLogeo (req, res, next){
    let find = data.users.findIndex(find => find.id === parseInt(req.headers.iduser,10));
    if(find>=0){
        let idxAux = parseInt(req.headers.iduser);
        let login = data.users.find(login => login.id == idxAux && login.login == true);
        if(login){
            next();
        }else{
            return res.status(400).send('Usuario no esta logueado');
        }
    }else{
        return res.status(500).send('No esta registrado');
    }
}

//VALIDACION DE USUARIO ADMINISTRADOR
function validarAdmin (req, res, next){
    let idxAux = req.headers.iduser;
    let admin = data.users.find(admin => admin.id == idxAux && admin.admin == true);
    if(admin){
        next();
    }else{
        return res.status(400).send('No es administrador');
    }
}


module.exports = { validarLogeo, validarAdmin};