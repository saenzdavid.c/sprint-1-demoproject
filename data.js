//INFORMACION DE USUARIOS
const users = [{
    id: 1,
    nombre: 'admin',
    apellido: 'admin',
    email: 'superUser@admin',
    telefono: 12345678,
    userPass: '123123456',
    admin: true,
    login: false
}];

//INFORMACION DE PRODUCTOS
const products = [{
    id: 1,
    nombre: 'Bagel de salmón',
    precio: 425
},{
    id: 2,
    nombre: 'Hamburguesa Clasica',
    precio: 350
}];

//INFORMACION DE PEDIDOS
const orders = [];

//INFORMACION DE  METODOS DE PAGO
const payMethod = [{
    id: 1,
    metodo:'efectivo'
},{
    id:2,
    metodo:'T.debito'
},{
    id:3,
    metodo:'T.credito'
}];

//ESTADOS DE LOS PEDIDOS
const estado = ['pendiente','confirmado','en preparacion','enviado','entregado'];

module.exports = {users,products,orders,payMethod,estado};