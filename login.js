const express = require ('express');
const router = express.Router();
const usuarios = require('./data');

//LOGUEO DE USUARIO
router.post('/', (req, res) => {
    const emailAux = usuarios.users.find(emailAux => emailAux.email === req.body.email);
    if(!emailAux){
        return  res.status(400).send('Usuario incorrecto o no registrado');
    }

    const login = usuarios.users.find(login => login.email === req.body.email && login.userPass === req.body.userPass);
    if(login){
        login.login = true;
        res.send('Usuario Logueado');
    }else{
        return  res.status(400).send('Contraseña incorrecta');
    }
})

module.exports = router;