const express = require ('express');
const router = express.Router();
const middleware = require('./middleware');
const data = require('./data');

//CONFIRMACION DEL PEDIDO
router.post('/', middleware.validarLogeo, (req, res) =>{
    let idxUser = data.users.findIndex(idxUser => idxUser.id === parseInt(req.headers.iduser,10));
    let idxOrder = data.orders.findIndex(idxOrder => idxOrder.id === parseInt(req.params.idorder,10));

    if(idxOrder<0) return res.status(400).send('El pedido no existe');
    
    if(data.orders[idxOrder].usuario == data.users[idxUser].nombre){
        data.orders[idxOrder].estado == data.estado[1]
        res.status(200).send('Pedido confirmado');
    }
    else return res.status(400).send('El pedido no corresponde al usuario');
});


module.exports = router;