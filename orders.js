const express = require ('express');
const router = express.Router();
const middleware = require('./middleware');
const data = require('./data');

//MUESTRA TODOS LOS PEDIDOS (USUARIO ADMINISTRADOR) O EL PEDIDO DEL USUARIO
router.get('/', middleware.validarLogeo, (req, res) => {
    let idxAux = data.users.findIndex(idxAux => idxAux.id === parseInt(req.headers.iduser,10));
    
    if(data.users[idxAux].admin==true){
        res.status('200').json({'Pedidos': data.orders});
    }
    else{
        let pedidoAux = data.orders.find(pedidoAux => pedidoAux.usuario === data.users[idxAux].nombre);
        if(pedidoAux){
            res.status(200).json({'Su pedido': pedidoAux});
        }
        else return res.status(400).send("No tiene pedidos cargados");
    }
})

//CREA UN PEDIDO NUEVO
router.post('/', middleware.validarLogeo, (req, res) => {
    let idxAux = data.users.findIndex(idxAux => idxAux.id === parseInt(req.headers.iduser,10));
    let paymet = parseInt(req.body.metodo);
    let newId = data.orders.length;
    let compraAux = req.body.item;
    let listaProductos = data.products
    let flag=0;

    if(newId > 0) newId = data.orders[data.orders.length-1].id;
    
    compraAux.forEach( item => {
        if(item.cantidad<=0){
            return  res.send("Se debe cargar al menos 1 producto");
        }
        else{
            listaProductos.forEach(producto => {
                if(producto.id == item.idproducto){
                    flag++
                }
            })
        }
    });
    if(flag != compraAux.length){
        return res.status(400).json('Ingrese un producto de la lista de productos');
    }
    else{
        data.orders.push({
            id: newId+1,
            estado: data.estado[0],
            compra: compraAux,
            metodoPago: data.payMethod[paymet-1],
            usuario: data.users[idxAux].nombre,
            direccion:req.body.nuevadireccion
        });
        res.status(201).send('Su pedido fue cargado con exito');
    }

})

//MODIFICA UN PEDIDO
router.put('/:id',middleware.validarLogeo, (req, res) => {
    const id =  parseInt(req.params.id);
    let compraAux = req.body.item;
    let listaProductos = data.products
    let flag=0;
    
    let idxOrder = data.orders.findIndex(idxAux => idxAux.id === id);
    let idxUser = data.users.findIndex(idxUser => idxUser.id === parseInt(req.headers.iduser,10));

    if(idxOrder<0) return res.status(400).send('Pedido Inexistente');
    
    compraAux.forEach( item => {
        if(item.cantidad<=0){
            return  res.send("Se debe cargar al menos 1 producto");
        }
        else{
            listaProductos.forEach(producto => {
                if(producto.id == item.idproducto){
                    flag++
                }
            })
        }
    });
    
    if(flag != compraAux.length){
        return res.status(400).json('Ingrese un producto de la lista de productos');
    }

    if(data.users[idxUser].admin==true){
        data.orders[idxOrder].estado = data.estado[req.body.estado-1];
    }
    else{
        if(data.users[idxUser].nombre != data.orders[idxOrder].usuario) return res.status(400).send('No puede modificar un pedido ajeno');
        if(data.orders[idxOrder].estado==data.estado[0] && data.orders[idxOrder].usuario==data.users[idxUser].nombre){
            data.orders[idxOrder].compra=req.body.item
            data.orders[idxOrder].metodoPago=data.payMethod[parseInt(req.body.metodo)-1]
            data.orders[idxOrder].direccion=req.body.nuevadireccion
        }
        else return res.status(400).send('Pedido ya confirmado, no se puede modificar')
    }
    
    res.status(200).send('Su pedido fue modificado con exito');

})

module.exports = router;