const express = require ('express');
const router = express.Router();
const middleware = require('./middleware');
const data = require('./data');

//MUESTRA LOS DISTINTOS METODOS DE PAGO
router.get('/', middleware.validarLogeo, (req, res) => {
    res.status('200').json({'Metodos de pago':data.payMethod});
})
  
//CREA UN NUEVOS METODO DE PAGO
router.post('/',middleware.validarLogeo, middleware.validarAdmin, (req, res) => {
    let newId = data.payMethod.length;
    
    if(newId > 0) newId = data.payMethod[data.payMethod.length-1].id;
    
    data.payMethod.push({
        id: newId+1,
        metodo: req.body.metodo
    });
    res.status('201').send('Metodo pago creado');
    
});
  
//MODIFICA UN METODO DE PAGO
router.put('/',middleware.validarLogeo, middleware.validarAdmin, (req, res) => {
    let idxAux = data.payMethod.findIndex(idxAux => idxAux.id === parseInt(req.headers.iduser,10));
    if(idxAux>=0){
        data.payMethod[idxAux].metodo = req.body.metodo;
        res.status('200').send('Metodo pago modificado');
    }
    else return res.status(500).send('Metodo de pago inexistente');
})
  
// ELIMINA UN METODO DE PAGO 
router.delete('/',middleware.validarLogeo, middleware.validarAdmin, (req, res) => {
    let idxAux = data.payMethod.findIndex(idxAux => idxAux.id === parseInt(req.headers.iduser));
    if(idxAux>=0){
        data.payMethod.splice(idxAux, 1);
        res.status('200').send('Metodo pago eliminado');
    }
    else return res.status(500).send('Metodo de pago inexistente');
})

module.exports = router;